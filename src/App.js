import logo from './logo.svg';
import './App.css';

function App() {
  const weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];

  const arrayOfWeekdays = weekdays.map((weekday, position)=>{
    return <tr><td key={position}>{weekday}</td></tr>
  })
  return (
    <div className="App">
      <hr/>
      <table>
        <thead>
          <tr><th>Weekday</th></tr>
        </thead>

        <tbody>
        {arrayOfWeekdays}
        </tbody>
      </table>
    </div>
  );
}

export default App;
